const express = require('express')
const cors = require('cors')
const passport = require('passport')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const path = require('path')
const helmet = require('helmet')
const hpp = require('hpp')
const RedisStore = require('connect-redis')(session)
const redisClient = require('./redisClient')
const morganMiddleware = require('./morganMiddleware')
const Logger = require('./logger')
const postRouter = require('./routes/post')
const userRouter = require('./routes/user')
const authRouter = require('./routes/auth')
const postsRouter = require('./routes/posts')
const seriesRouter = require('./routes/series')
const uploadRouter = require('./routes/upload')
const searchRouter = require('./routes/search')
const commentRouter = require('./routes/comment')
const commentsRouter = require('./routes/comments')
const hashtagsRouter = require('./routes/hashtags')
const indexRouter = require('./routes')
const passportConfig = require('./passport')
const errorHandler = require('./custom-error-handler')

require('dotenv').config()

const corsOptions = {
  origin: ['http://localhost:3000', 'https://broccolidb.com'],
  credentials: true
}

const sessionOpt = {
  resave: false,
  saveUninitialized: false,
  secret: process.env.SECRET,
  cookie: {
    httpOnly: true,
    secure: false,
    sameSite: 'lax',
    domain: process.env.NODE_ENV === 'production' && '.broccolidb.com'
  }
}
const app = express()
const db = require('./models')

db.sequelize
  .sync()
  .then(() => {
    Logger.debug('✅ db connected')
  })
  .catch((e) => {
    Logger.error(e)
  })
app.use(morganMiddleware)
app.use(cors(corsOptions))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser(process.env.SECRET))
if (process.env.NODE_ENV === 'production') {
  sessionOpt.proxy = true
  sessionOpt.cookie.secure = true
  sessionOpt.store = new RedisStore({ client: redisClient })
  app.use(helmet())
  app.use(hpp())
}
app.use(session(sessionOpt))
app.use(passport.initialize())
app.use(passport.session())
passportConfig()
app.use('/', express.static(path.join(__dirname, 'uploads')))
app.use('/', indexRouter)
app.use('/auth', authRouter)
app.use('/post', postRouter)
app.use('/user', userRouter)
app.use('/posts', postsRouter)
app.use('/upload', uploadRouter)
app.use('/series', seriesRouter)
app.use('/search', searchRouter)
app.use('/comment', commentRouter)
app.use('/comments', commentsRouter)
app.use('/hashtags', hashtagsRouter)
app.use('*', (req, res, next) => {
  res.status(404).send('NOT FOUND PAGE')
})
app.use(errorHandler)
app.use((err, req, res, next) => {
  if (err) {
    Logger.error('🥶🥶🥶', err)
    return res.status(500).json({ message: err.message, stack: err.stack })
  }
})

app.listen(process.env.PORT, () => {
  Logger.debug(`✅server on ${process.env.PORT}`)
})
