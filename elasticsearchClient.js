const { Client } = require('@elastic/elasticsearch')
const Logger = require('./logger')
require('dotenv').config()

const elasticNodeURL =
  process.env.NODE_ENV === 'production'
    ? process.env.ELASTIC_PROD_URL
    : process.env.ELASTIC_DEV_URL
const clientInfo = {
  node: elasticNodeURL
}
if (process.env.NODE_ENV === 'production') {
  clientInfo.auth = {
    username: process.env.ELASTIC_USER,
    password: process.env.ELASTIC_PASSWORD
  }
}
const client = new Client(clientInfo)

const getSearchTermTokens = async (term) => {
  try {
    const analyzer =
      process.env.NODE_ENV === 'production'
        ? 'broccoli_eunjeon'
        : 'broccoli_nori_discard'
    const result = await client.indices.analyze({
      index: 'whale',
      body: {
        analyzer,
        text: term
      }
    })
    return result
  } catch (err) {
    if (err) {
      Logger.error(err)
    }
  }
}

const updatePost = async (post, user) => {
  const {
    id,
    title,
    content,
    isDeleted: isdeleted,
    updatedAt: updatedat,
    thumbnail,
    isUse: isuse,
    scope
  } = post
  const { nickname, avatar, defaultavatar } = user
  try {
    const result = await client.update({
      index: 'whale',
      id,
      body: {
        doc: {
          title,
          isdeleted,
          content,
          updatedat,
          thumbnail,
          isuse,
          scope,
          nickname,
          avatar,
          defaultavatar
        }
      }
    })
    return result
  } catch (err) {
    if (err) {
      Logger.error(err)
    }
  }
}

const putPost = async (post, user) => {
  const {
    id,
    title,
    content,
    postKey: postkey,
    isDeleted: isdeleted,
    updatedAt: updatedat,
    thumbnail,
    isUse: isuse,
    scope
  } = post
  const { nickname, avatar, defaultavatar, id: userId } = user
  try {
    const result = await client.index({
      index: 'whale',
      id,
      body: {
        title,
        avatar,
        nickname,
        postkey,
        isdeleted,
        content,
        updatedat,
        defaultavatar,
        thumbnail,
        isuse,
        scope,
        userId
      }
    })
    return result
  } catch (err) {
    if (err) {
      Logger.error(err)
    }
  }
}

const getSearchPosts = async (terms, username, userId) => {
  try {
    const must = [
      {
        multi_match: {
          query: `${terms}`,
          fields: ['content', 'title']
        }
      },
      {
        match: { isdeleted: false }
      },
      {
        match: { scope: 'A' }
      }
    ]
    if (username) {
      const match = {}
      if (userId) {
        match.userId = userId
      } else {
        match.nickname = username
      }
      must.splice(must.length - 1, 1, { match })
    }

    const result = await client.search({
      index: 'whale',
      body: {
        query: {
          bool: {
            must
          }
        }
      }
    })
    return result
  } catch (err) {
    if (err) {
      Logger.error(err)
    }
  }
}

module.exports = {
  getSearchPosts,
  putPost,
  updatePost,
  getSearchTermTokens
}
