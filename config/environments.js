require('dotenv').config()

const FRONT_URL =
  process.env.NODE_ENV === 'production'
    ? process.env.PROD_FRONT_URL
    : process.env.DEV_FRONT_URL

const BACK_URL =
  process.env.NODE_ENV === 'production'
    ? process.env.PROD_BACK_URL
    : process.env.DEV_BACK_URL

module.exports = {
  BACK_URL,
  FRONT_URL
}
