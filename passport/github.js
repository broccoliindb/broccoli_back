const passport = require('passport')
const GithubStrategy = require('passport-github2').Strategy
const fetch = require('node-fetch')
const Logger = require('../logger')
const { User, UserDetail } = require('../models')
const { BACK_URL } = require('../config/environments')

require('dotenv').config()

module.exports = () => {
  passport.use(
    new GithubStrategy(
      {
        clientID: process.env.GITHUB_CLIENT_ID,
        clientSecret: process.env.GITHUB_CLIENT_SECRET,
        callbackURL: `${BACK_URL}/auth/github/callback`
      },
      async (accessToken, refreshToken, profile, done) => {
        try {
          const {
            _json: { id, email, avatar_url: profileImage },
            username
          } = profile
          const user = await User.findOne({
            where: { githubId: id },
            attributes: [
              'id',
              'nickname',
              'avatar',
              'email',
              'defaultavatar',
              'visitCount'
            ],
            include: [{ model: UserDetail, attributes: ['whoami'] }]
          })
          if (user) {
            await User.update(
              {
                visitCount: user.visitCount + 1
              },
              {
                where: { id: user.id, githubId: id }
              }
            )
            const updatedUser = await User.findOne({
              where: { id: user.id, githubId: id },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ],
              include: [{ model: UserDetail, attributes: ['whoami'] }]
            })
            return done(null, updatedUser)
          }
          if (!user) {
            let foundEmail = null
            if (!email) {
              const emailData = await (
                await fetch('https://api.github.com/user/emails', {
                  headers: {
                    Authorization: `token ${accessToken}`
                  }
                })
              ).json()
              foundEmail = emailData.find(
                (mail) => mail.primary === true && mail.verified === true
              )
            }
            const userWithSameEmail = await User.findOne({
              where: { email: email || foundEmail.email },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ]
            })
            if (userWithSameEmail) {
              await User.update(
                {
                  githubId: id,
                  visitCount: userWithSameEmail.visitCount + 1
                },
                {
                  where: { email: foundEmail.email, id: userWithSameEmail.id }
                }
              )
              const updatedUser = await User.findOne({
                where: { id: userWithSameEmail.id },
                attributes: [
                  'id',
                  'nickname',
                  'avatar',
                  'email',
                  'defaultavatar',
                  'visitCount'
                ],
                include: [{ model: UserDetail, attributes: ['whoami'] }]
              })
              return done(null, updatedUser)
            }
            const createdUser = await User.create({
              githubId: profile.id,
              email: foundEmail.email,
              nickname: username,
              defaultavatar: profileImage,
              visitCount: 1
            })
            const newUser = await User.findOne({
              where: { id: createdUser.id },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ],
              include: [{ model: UserDetail, attributes: ['whoami'] }]
            })
            return done(null, newUser)
          }
        } catch (error) {
          if (error) {
            Logger.error(error)
            done(error)
          }
        }
      }
    )
  )
}
