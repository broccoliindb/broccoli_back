const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { compare } = require('bcrypt')
const Logger = require('../logger')
const { User } = require('../models')

module.exports = () => {
  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password'
      },
      async (email, password, done) => {
        try {
          const user = await User.findOne({
            where: { email },
            attributes: ['id', 'password', 'visitCount', 'isActive']
          })
          if (!user) {
            return done(null, false, { message: '해당사용자가 없습니다.' })
          }
          if (!user.isActive) {
            return done(null, false, {
              message: '비밀번호 변경버튼을 통한 이메일인증이 필요합니다.'
            })
          }
          if (!user.password) {
            return done(null, false, {
              message: '이메일로 가입한 유저가 아닙니다.'
            })
          }
          const result = await compare(password, user.password)
          if (result) {
            return done(null, user)
          }
          return done(null, false, { message: '사용자 정보가 잘못됐습니다.' })
        } catch (error) {
          if (error) {
            Logger.error(error)
            done(error)
          }
        }
      }
    )
  )
}
