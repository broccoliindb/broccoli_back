const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy
const { User, UserDetail } = require('../models')
const { BACK_URL, FRONT_URL } = require('../config/environments')
const Logger = require('../logger')
require('dotenv').config()

module.exports = () => {
  passport.use(
    new FacebookStrategy(
      {
        clientID: process.env.FB_CLIENT_ID,
        clientSecret: process.env.FB_CLIENT_SECRET,
        callbackURL: `${BACK_URL}/auth/facebook/callback`,
        profileFields: ['id', 'displayName', 'photos', 'email']
      },
      async (accessToken, refreshToken, profile, done) => {
        try {
          const {
            _json: { id, email, name }
          } = profile
          const user = await User.findOne({
            where: { facebookId: id },
            attributes: [
              'id',
              'nickname',
              'avatar',
              'email',
              'defaultavatar',
              'visitCount'
            ],
            include: [{ model: UserDetail, attributes: ['whoami'] }]
          })
          if (user) {
            await User.update(
              {
                visitCount: user.visitCount + 1
              },
              {
                where: { id: user.id, facebookId: id }
              }
            )
            const updatedUser = await User.findOne({
              where: { id: user.id, facebookId: id },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ],
              include: [{ model: UserDetail, attributes: ['whoami'] }]
            })
            return done(null, updatedUser)
          }
          if (!user) {
            const userWithSameEmail = await User.findOne({
              where: { email },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ]
            })
            if (userWithSameEmail) {
              await User.update(
                {
                  visitCount: userWithSameEmail.visitCount + 1,
                  facebookId: id
                },
                {
                  where: {
                    email,
                    id: userWithSameEmail.id
                  }
                }
              )
              const updatedUser = await User.findOne({
                where: { id: userWithSameEmail.id },
                attributes: [
                  'id',
                  'nickname',
                  'avatar',
                  'email',
                  'defaultavatar',
                  'visitCount'
                ],
                include: [{ model: UserDetail, attributes: ['whoami'] }]
              })
              return done(null, updatedUser)
            }
            const createdUser = await User.create({
              facebookId: profile.id,
              email,
              nickname: name,
              defaultavatar: `${FRONT_URL}/nouser.png`,
              visitCount: 1
            })
            const newUser = await User.findOne({
              where: { id: createdUser.id },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ],
              include: [{ model: UserDetail, attributes: ['whoami'] }]
            })
            return done(null, newUser)
          }
        } catch (error) {
          if (error) {
            Logger.error(error)
            done(error)
          }
        }
      }
    )
  )
}
