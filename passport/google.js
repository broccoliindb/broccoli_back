const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
const { User, UserDetail } = require('../models')
const Logger = require('../logger')
const { BACK_URL } = require('../config/environments')
require('dotenv').config()

module.exports = () => {
  passport.use(
    new GoogleStrategy(
      {
        clientID: process.env.GG_CLIENT_ID,
        clientSecret: process.env.GG_CLIENT_SECRET,
        callbackURL: `${BACK_URL}/auth/google/callback`,
        passReqToCallback: true
      },
      async (req, assessToken, refreshToken, profile, done) => {
        try {
          const {
            id,
            displayName,
            _json: { picture, email }
          } = profile
          const user = await User.findOne({
            where: { googleId: id },
            attributes: [
              'id',
              'nickname',
              'avatar',
              'email',
              'defaultavatar',
              'visitCount'
            ],
            include: [{ model: UserDetail, attributes: ['whoami'] }]
          })
          if (user) {
            await User.update(
              {
                visitCount: user.visitCount + 1
              },
              {
                where: { id: user.id, googleId: id }
              }
            )
            const updatedUser = await User.findOne({
              where: { id: user.id, googleId: id },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ],
              include: [{ model: UserDetail, attributes: ['whoami'] }]
            })
            return done(null, updatedUser)
          }
          if (!user) {
            const userWithSameEmail = await User.findOne({
              where: { email },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ]
            })
            if (userWithSameEmail) {
              await User.update(
                {
                  googleId: id,
                  visitCount: userWithSameEmail.visitCount + 1
                },
                {
                  where: {
                    email,
                    id: userWithSameEmail.id
                  }
                }
              )
              const updatedUser = await User.findOne({
                where: { id: userWithSameEmail.id },
                attributes: [
                  'id',
                  'nickname',
                  'avatar',
                  'email',
                  'defaultavatar',
                  'visitCount'
                ],
                include: [{ model: UserDetail, attributes: ['whoami'] }]
              })
              return done(null, updatedUser)
            }
            const createdUser = await User.create({
              googleId: id,
              email,
              nickname: displayName,
              defaultavatar: picture,
              visitCount: 1
            })
            const newUser = await User.findOne({
              where: { id: createdUser.id },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ],
              include: [{ model: UserDetail, attributes: ['whoami'] }]
            })
            return done(null, newUser)
          }
        } catch (error) {
          if (error) {
            Logger.error(error)
            done(error)
          }
        }
      }
    )
  )
}
