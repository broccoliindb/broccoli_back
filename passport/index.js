const passport = require('passport')
const { User } = require('../models')
const local = require('./local')
const github = require('./github')
const google = require('./google')
const kakao = require('./kakao')
const facebook = require('./facebook')
const Logger = require('../logger')

module.exports = () => {
  passport.serializeUser((user, done) => {
    done(null, user.id)
  })
  passport.deserializeUser(async (id, done) => {
    try {
      const user = await User.findOne({
        where: { id },
        attributes: [
          'id',
          'nickname',
          'avatar',
          'email',
          'defaultavatar',
          'visitCount'
        ]
      })
      done(null, user)
    } catch (error) {
      if (error) {
        Logger.error(error)
        done(error)
      }
    }
  })
  local()
  github()
  google()
  kakao()
  facebook()
}
