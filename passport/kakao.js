const passport = require('passport')
const KakaoStrategy = require('passport-kakao').Strategy
const { BACK_URL } = require('../config/environments')
const { User, UserDetail } = require('../models')
const Logger = require('../logger')
require('dotenv').config()

module.exports = () => {
  passport.use(
    new KakaoStrategy(
      {
        clientID: process.env.KO_CLIENT_ID,
        clientSecret: process.env.KO_CLIENT_SECRET,
        callbackURL: `${BACK_URL}/auth/kakao/callback`
      },
      async (accessToken, refreshToken, profile, done) => {
        try {
          const {
            id,
            _json: {
              kakao_account: {
                email,
                profile: { nickname, profile_image_url: profileImage }
              }
            }
          } = profile
          const user = await User.findOne({
            where: { kakaoId: id },
            attributes: [
              'id',
              'nickname',
              'avatar',
              'email',
              'defaultavatar',
              'visitCount'
            ],
            include: [{ model: UserDetail, attributes: ['whoami'] }]
          })
          if (user) {
            await User.update(
              {
                visitCount: user.visitCount + 1
              },
              {
                where: { id: user.id, kakaoId: id }
              }
            )
            const updatedUser = await User.findOne({
              where: { id: user.id, kakaoId: id },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ],
              include: [{ model: UserDetail, attributes: ['whoami'] }]
            })
            return done(null, updatedUser)
          }
          if (!user) {
            if (!email) {
              return done(null, null, {
                message: 'email is not found',
                redirect: `https://kauth.kakao.com/oauth/authorize?client_id=${process.env.KO_CLIENT_ID}&redirect_uri=${BACK_URL}/auth/kakao/callback&response_type=code&scope=account_email`
              })
            }
            const userWithSameEmail = await User.findOne({
              where: { email },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ]
            })
            if (userWithSameEmail) {
              await User.update(
                {
                  kakaoId: id,
                  visitCount: userWithSameEmail.visitCount + 1
                },
                {
                  where: {
                    email,
                    id: userWithSameEmail.id
                  }
                }
              )
              const updatedUser = await User.findOne({
                where: { id: userWithSameEmail.id },
                attributes: [
                  'id',
                  'nickname',
                  'avatar',
                  'email',
                  'defaultavatar',
                  'visitCount'
                ],
                include: [{ model: UserDetail, attributes: ['whoami'] }]
              })
              return done(null, updatedUser)
            }
            const createdUser = await User.create({
              kakaoId: id,
              email,
              nickname,
              defaultavatar: profileImage,
              visitCount: 1
            })
            const newUser = await User.findOne({
              where: { id: createdUser.id },
              attributes: [
                'id',
                'nickname',
                'avatar',
                'email',
                'defaultavatar',
                'visitCount'
              ],
              include: [{ model: UserDetail, attributes: ['whoami'] }]
            })
            return done(null, newUser)
          }
        } catch (error) {
          if (error) {
            Logger.error(error)
            done(error)
          }
        }
      }
    )
  )
}
