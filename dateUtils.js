const getLastDateOfMonth = (today) => {
  const t = today || new Date()
  return new Date(t.getFullYear(), t.getMonth() + 1, 0).getDate()
}

const addMonth = (today, add) => {
  const t =
    (today && new Date(today.setDate(1))) || new Date(new Date().setDate(1))
  const adder = add || 0
  t.setMonth(t.getMonth() + adder)
  return t
}

const addYear = (today, add) => {
  const t =
    (today && new Date(today.setDate(1))) || new Date(new Date().setDate(1))
  const adder = add || 0
  t.setYear(t.getFullYear() + adder)
  return t
}

const getFirstLastDateOfMonth = (today) => {
  const t = today || new Date()
  const startDate = 1
  const startMonth = t.getMonth()
  const startYear = t.getFullYear()
  const endDate = getLastDateOfMonth(t)
  const endMonth = t.getMonth()
  const endYear = t.getFullYear()
  return [
    new Date(startYear, startMonth, startDate),
    new Date(endYear, endMonth, endDate)
  ]
}

const getFirstLastDateOfWeek = (today) => {
  const t = today || new Date()
  const lastDate = getLastDateOfMonth(t)
  let startDate = t.getDate() - t.getDay()
  let startMonth = t.getMonth()
  let startYear = t.getFullYear()
  let endDate = startDate + 7
  let endMonth = t.getMonth()
  let endYear = t.getFullYear()
  if (startDate < 1) {
    startDate += getLastDateOfMonth(addMonth(t, -1))
    startMonth = addMonth(t, -1).getMonth()
    if (startMonth > t.getMonth()) {
      startYear = addYear(t, -1).getFullYear()
    }
  }
  if (lastDate < endDate) {
    endDate -= lastDate
    endMonth = addMonth(t, 1).getMonth()
    if (endMonth < t.getMonth()) {
      endYear = addYear(t, 1).getFullYear()
    }
  }
  return [
    new Date(startYear, startMonth, startDate),
    new Date(endYear, endMonth, endDate)
  ]
}

const getFirstLastOfDate = (today) => {
  const t = today || new Date()
  const lastDate = getLastDateOfMonth(t)
  const startDate = t.getDate()
  const startMonth = t.getMonth()
  const startYear = t.getFullYear()
  let endDate = startDate + 1
  let endMonth = t.getMonth()
  let endYear = t.getFullYear()
  if (lastDate < endDate) {
    endDate -= lastDate
    endMonth = addMonth(t, 1).getMonth()
    if (endMonth < addMonth(t, -1).getMonth()) {
      endYear = addYear(t, 1).getFullYear()
    }
  }
  return [
    new Date(startYear, startMonth, startDate),
    new Date(endYear, endMonth, endDate)
  ]
}

module.exports = {
  getFirstLastDateOfMonth,
  getFirstLastDateOfWeek,
  getFirstLastOfDate,
  getLastDateOfMonth,
  addMonth,
  addYear
}
