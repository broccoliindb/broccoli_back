const redis = require('redis')
const Logger = require('./logger')
require('dotenv').config()

let redisClient = ''
if (process.env.NODE_ENV === 'production') {
  redisClient = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASSWORD
  })
  /*
   * Calling unref() will allow this program to exit immediately after the get
   * command finishes. Otherwise the client would hang as long as the
   * client-server connection is alive.
   */
  redisClient.unref()
  redisClient.on('ready', () => {
    Logger.debug('✅ redis is ready')
  })
  redisClient.on('connect', () => {
    Logger.debug('✅ redis is connected')
  })
  redisClient.on('reconnecting', () => {
    Logger.debug('✅ redis is reconnecting')
  })
  redisClient.on('end', () => {
    Logger.debug('✅ redis is end')
  })
  redisClient.on('error', (error) => {
    Logger.error(error)
  })
}

module.exports = redisClient
