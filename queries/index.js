const getRootCommentStackQueryById = (id) => {
  const query = `
    with recursive stack_comment_tree as(
        with recursive findroot_comment as(
            select id, userId, upperId, content, postId, updatedAt, isDeleted
            from comments
            where id = ${id}
            union all
            select c.id, c.userId, c.upperId, c.content, c.postId, c.updatedAt, c.isDeleted
            from comments c
            inner join findroot_comment cte on cte.upperId = c.id
            where c.isDeleted = false
        )
        select id, userId, upperId, content, postId, updatedAt, isDeleted, 1 lvl
        from findroot_comment 
        where upperId is null
        union all
        select c.id, c.userId, c.upperId, c.content, c.postId, c.updatedAt, c.isDeleted, lvl + 1
        from comments c
        inner join stack_comment_tree cte on cte.id = c.upperId
        where c.isDeleted = false
    )
    select cte.id, cte.content, cte.upperId, cte.userId, cte.postId, cte.updatedAt, cte.lvl, cte.isDeleted, u.nickname, COALESCE(u.avatar, u.defaultavatar) avatar
    from stack_comment_tree cte inner join users u on cte.userId = u.id 
    order by lvl, id desc
    `
  return query
}

const getPostCommentsStackQueryByPostKey = (postKey) => {
  const query = `
    with recursive comments_cte as (
        select c.id, c.content, c.upperId, c.userId, c.postId, c.updatedAt, 1 lvl
        from comments c
        inner join posts p on p.id = c.postId and p.postKey='${postKey}'
        where c.upperId is null and c.isDeleted = false
        union all
        select c.id, c.content, c.upperId, c.userId, c.postId, c.updatedAt, lvl+1
        from comments c
        inner join comments_cte cte on cte.id = c.upperId
        where c.isDeleted = false
    )
    select c.id, c.content, c.upperId, c.userId, c.postId, c.updatedAt, c.lvl, u.nickname, COALESCE(u.avatar, u.defaultavatar) avatar
    from comments_cte c
    inner join users u on u.id = c.userId
    order by lvl, id desc
  `
  return query
}

const getHashtagsQueryForNickname = (nickname) => {
  const query = `
    select h.id hashtagId, h.tag hashtag, count(1) count
    from posts p
    inner join users u on p.userId = u.id
    inner join posthashtags t on p.id = t.postId and t.isUse = true
    inner join hashtags h on h.id = t.hashtagId
    where u.nickname = '${nickname}' and p.isUse = true
    group by h.tag, h.id
    `
  return query
}

const getAllSeriesOnPostsQueryForNickname = (nickname, updatedAt = null) => {
  const query = `
    select COALESCE(s.id, 0) seriesId, COALESCE(s.name, '시리즈없음') seriesName, p2.count seriesCount, p1.thumbnail , p1.updatedAt
      from posts p1
      inner join (
          select count(1) count ,p.seriesId, max(p.updatedAt) updatedAt
          from posts p
          inner join users u on p.userId = u.id
          where u.nickname = '${nickname}'
          group by p.seriesId
      ) p2 on p1.updatedAt = p2.updatedAt
      left join series s on p1.seriesId = s.id
      where p1.updatedAt < COALESCE(convert('${updatedAt}', datetime), SYSDATE())
      and p1.isDeleted = false
      and p1.isUse = true
      order by p1.updatedAt desc
      limit 10
  `
  return query
}

const getPostMetaListOnSeriesQueryByPostKey = (postKey) => {
  const query = `
    select 
        p2.id postId, 
        p2.title postTitle, 
        p2.postKey, 
        p2.seriesId, 
        s.name seriesName
    from (
      select p.seriesId
      from posts p
      where p.isDeleted = false 
      and p.isUse = true 
      and p.postKey = '${postKey}'
    ) p1
    inner join posts p2 on p2.seriesId = p1.seriesId
    inner join series s on s.id = p1.seriesId
    where p2.isDeleted = false and p2.isUse = true
  `
  return query
}

const getAllPostsOnConditionQueryByLikeCountAndLastId = (
  likeCount,
  lastId,
  startDate,
  endDate
) => {
  const whereWithDuration =
    startDate && endDate
      ? `where p.isDeleted = false and p.isUse = true and p.scope = 'A' and updatedAt >= '${startDate}' and updatedAt < '${endDate}'`
      : 'where p.isDeleted = false and p.isUse = true and p.scope = "A"'
  const whereWithLikeCount = Number.isInteger(likeCount)
    ? `where p2.likeCount <=${likeCount} `
    : 'where 1 = 1'
  const whereWithlastId = Number.isInteger(likeCount)
    ? `where (Post.likeCount = ${likeCount} and Post.id > ${lastId}) or (Post.likeCount < ${likeCount})`
    : 'where 1 = 1'
  const query = `
      select
          Post.id,
          Post.title,
          Post.content,
          Post.updatedAt,
          Post.postKey,
          Post.thumbnail,
          Post.likeCount as 'Liker.count',          
          User.id as 'User.id',
          User.nickname as 'User.nickname',
          COALESCE(User.avatar , User.defaultavatar ) as 'User.avatar',
          (select count(1) from comments c where c.postId = Post.id) 'Comment.count',
          group_concat(h.id) 'Hashtag.id',
          group_concat(h.tag) 'Hashtag.tag'
      from (
          select p2.title, p2.likeCount, p2.id, p2.userId, p2.content, p2.updatedAt, p2.postKey, p2.thumbnail
          from (
              select
              p.title,
              p.content,
              p.updatedAt,
              p.postKey,
              p.thumbnail,
              (
                select count(1) from likes l where l.postId = p.id
              ) likeCount,
              p.id,
              p.userId
              from posts p
              ${whereWithDuration}
          ) p2
          ${whereWithLikeCount}
      ) Post
      inner join users User on User.id = Post.userId 
      left join (posthashtags postTag inner join hashtags h on h.id = postTag.hashtagId and postTag.isUse = true) on postTag.postId = Post.id
      ${whereWithlastId}
      group by Post.id, Post.title, Post.likeCount, Post.updatedAt, Post.content, Post.postKey, Post.thumbnail, User.id, User.nickname, 'User.avatar', 'Comment.count'
      order by Post.likeCount desc, Post.id
      limit 10;
  `
  return query
}

const getAllPostsOnConditionQueryByDateAndLastId = (
  lastId,
  startDate,
  endDate
) => {
  const lastIdCodition = lastId === 0 ? '1 = 1' : `Post.id < ${lastId}`
  const whereWithDuration =
    startDate && endDate
      ? `where Post.isDeleted = false and Post.isUse = true and Post.scope = 'A' and Post.updatedAt >= '${startDate}' and Post.updatedAt < '${endDate}' and ${lastIdCodition}`
      : `where Post.isDeleted = false and Post.isUse = true and Post.scope = 'A' and ${lastIdCodition}`
  const query = `
    select
      Post.id,
      Post.title,
      Post.content,
      Post.updatedAt,
      Post.postKey,
      Post.thumbnail,
      (select count(1) from likes l where l.postId = Post.id) as 'Liker.count',
      User.id as 'User.id',
      User.nickname as 'User.nickname',
      COALESCE(User.avatar , User.defaultavatar ) as 'User.avatar',
      (select count(1) from comments c where c.postId = Post.id) 'Comment.count',
      group_concat(h.id) 'Hashtag.id',
      group_concat(h.tag) 'Hashtag.tag'
    from posts Post
    inner join users User on User.id = Post.userId 
    left join (posthashtags postTag inner join hashtags h on h.id = postTag.hashtagId and postTag.isUse = true) on postTag.postId = Post.id
    ${whereWithDuration}
    group by Post.id, Post.title, Post.updatedAt, Post.content, Post.postKey, Post.thumbnail, User.id, User.nickname, 'User.avatar', 'Comment.count'
    order by Post.updatedAt desc, Post.id
    limit 10;  
  `
  return query
}

module.exports = {
  getRootCommentStackQueryById,
  getPostCommentsStackQueryByPostKey,
  getHashtagsQueryForNickname,
  getAllSeriesOnPostsQueryForNickname,
  getPostMetaListOnSeriesQueryByPostKey,
  getAllPostsOnConditionQueryByLikeCountAndLastId,
  getAllPostsOnConditionQueryByDateAndLastId
}
