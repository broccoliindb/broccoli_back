const DataTypes = require('sequelize')

const { Model } = DataTypes

module.exports = class User extends Model {
  static init(sequelize) {
    return super.init(
      {
        email: {
          type: DataTypes.STRING(30),
          allowNull: false,
          unique: true
        },
        password: {
          type: DataTypes.STRING(100),
          allowNull: true
        },
        nickname: {
          type: DataTypes.STRING(30),
          allowNull: true
        },
        githubId: {
          type: DataTypes.STRING(30),
          allowNull: true
        },
        googleId: {
          type: DataTypes.STRING(30),
          allowNull: true
        },
        kakaoId: {
          type: DataTypes.STRING(30),
          allowNull: true
        },
        facebookId: {
          type: DataTypes.STRING(30),
          allowNull: true
        },
        avatar: {
          type: DataTypes.STRING(200),
          allowNull: true
        },
        defaultavatar: {
          type: DataTypes.STRING(200),
          allowNull: false
        },
        visitCount: {
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 1
        },
        isActive: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true
        },
        confirmationCode: {
          type: DataTypes.STRING(200),
          allowNull: true
        },
        blogname: {
          type: DataTypes.STRING(10),
          allowNull: true
        }
      },
      {
        modelName: 'User',
        tableName: 'users',
        charset: 'utf8',
        collate: 'utf8_general_ci',
        sequelize
      }
    )
  }

  static associate(db) {
    db.User.hasMany(db.Post, {
      foreignKey: 'userId',
      sourceKey: 'id'
    })
    db.User.hasOne(db.UserDetail, {
      foreignKey: 'userId',
      sourceKey: 'id'
    })
    db.User.belongsToMany(db.Post, {
      as: 'Liked',
      through: 'likes',
      foreignKey: 'userId',
      sourceKey: 'id'
    })
    db.User.hasMany(db.Comment, {
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      sourceKey: 'id'
    })
    db.User.hasMany(db.Series, {
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      sourceKey: 'id'
    })
  }
}
