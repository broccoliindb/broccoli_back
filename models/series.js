const DataTypes = require('sequelize')

const { Model } = DataTypes

module.exports = class Series extends Model {
  static init(sequelize) {
    return super.init(
      {
        name: {
          type: DataTypes.STRING(10),
          allowNull: false
        }
      },
      {
        modelName: 'Series',
        tableName: 'series',
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
        sequelize
      }
    )
  }

  static associate(db) {
    db.Series.hasMany(db.Post, { foreignKey: 'seriesId', sourceKey: 'id' })
    db.Series.belongsTo(db.User, { foreignKey: 'userId', targetKey: 'id' })
  }
}
