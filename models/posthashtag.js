const DataTypes = require('sequelize')

const { Model } = DataTypes

module.exports = class Posthashtag extends Model {
  static init(sequelize) {
    return super.init(
      {
        isUse: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true
        },
        isTemp: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        isTempDeleted: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false
        }
      },
      {
        modelName: 'Posthashtag',
        tableName: 'posthashtags',
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
        sequelize
      }
    )
  }
}
