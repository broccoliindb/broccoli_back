const DataTypes = require('sequelize')

const { Model } = DataTypes

class Comment extends Model {
  static init(sequelize) {
    return super.init(
      {
        content: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        upperId: {
          type: DataTypes.INTEGER,
          allowNull: true
        },
        isDeleted: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false
        }
      },
      {
        modelName: 'Comment',
        tableName: 'comments',
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
        sequelize
      }
    )
  }

  static associate(db) {
    db.Comment.belongsTo(db.User, {
      foreignKey: 'userId',
      targetKey: 'id'
    })
    db.Comment.belongsTo(db.Post, {
      foreignKey: 'postId',
      targetKey: 'id'
    })
    db.Comment.hasMany(db.Comment, {
      as: 'LowerComment',
      foreignKey: {
        name: 'upperId',
        sourceKey: 'upperId'
      }
    })
    db.Comment.belongsTo(db.Comment, {
      as: 'UpperComment',
      foreignKey: {
        name: 'upperId',
        targetKey: 'upperId'
      }
    })
  }
}

module.exports = Comment
