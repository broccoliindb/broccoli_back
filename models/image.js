const DataTypes = require('sequelize')

const { Model } = DataTypes

module.exports = class Image extends Model {
  static init(sequelize) {
    return super.init(
      {
        src: {
          type: DataTypes.STRING(200),
          allowNull: false
        },
        size: {
          type: DataTypes.STRING(10),
          allowNull: false
        },
        mimetype: {
          type: DataTypes.STRING(20),
          allowNull: true
        },
        originalname: {
          type: DataTypes.STRING(20),
          allowNull: false
        },
        filename: {
          type: DataTypes.STRING(20),
          allowNull: false
        }
      },
      {
        modelName: 'Image',
        tableName: 'images',
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
        sequelize
      }
    )
  }

  static associate(db) {
    db.Image.belongsTo(db.Post, {
      foreignKey: 'postId',
      targetKey: 'id'
    })
  }
}
