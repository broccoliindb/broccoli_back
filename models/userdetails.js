const DataTypes = require('sequelize')

const { Model } = DataTypes

module.exports = class User extends Model {
  static init(sequelize) {
    return super.init(
      {
        whoami: {
          type: DataTypes.STRING(200),
          allowNull: true
        }
      },
      {
        modelName: 'UserDetail',
        tableName: 'userdetails',
        charset: 'utf8mb4',
        collate: 'utf8mb4_0900_ai_ci',
        sequelize
      }
    )
  }

  static associate(db) {
    db.UserDetail.belongsTo(db.User, {
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      targetKey: 'id'
    })
  }
}
