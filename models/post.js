const DataTypes = require('sequelize')

const { Model } = DataTypes

module.exports = class Post extends Model {
  static init(sequelize) {
    return super.init(
      {
        title: {
          type: DataTypes.STRING(100),
          allowNull: true
        },
        content: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        postKey: {
          type: DataTypes.STRING(100),
          allowNull: false,
          unique: true
        },
        markdown: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        isTemp: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        tempTitle: {
          type: DataTypes.STRING(100),
          allowNull: true
        },
        tempContent: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        tempMarkdown: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        isUse: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true
        },
        isDeleted: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        scope: {
          type: DataTypes.STRING(1),
          allowNull: false,
          defaultValue: 'A'
        },
        summary: {
          type: DataTypes.STRING(150),
          allowNull: true
        },
        series: {
          type: DataTypes.STRING(20),
          allowNull: true
        },
        thumbnail: {
          type: DataTypes.STRING(200),
          allowNull: true
        }
      },
      {
        modelName: 'Post',
        tableName: 'posts',
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
        sequelize
      }
    )
  }

  static associate(db) {
    db.Post.belongsTo(db.User, {
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      targetKey: 'id'
    })
    db.Post.hasMany(db.Image, {
      foreignKey: 'postId',
      sourceKey: 'id'
    })
    db.Post.belongsToMany(db.User, {
      as: 'Liker',
      through: 'likes',
      foreignKey: 'postId',
      sourceKey: 'id'
    })
    db.Post.hasMany(db.Comment, {
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      foreignKey: 'postId',
      sourceKey: 'id'
    })
    db.Post.belongsTo(db.Series, {
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      foreignKey: 'seriesId',
      targetKey: 'id'
    })
    db.Post.belongsToMany(db.Hashtag, {
      through: db.Posthashtag,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      foreignKey: 'postId',
      sourceKey: 'id'
    })
  }
}
