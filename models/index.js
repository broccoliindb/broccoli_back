const env = process.env.NODE_ENV || 'development'
const Sequelize = require('sequelize')
const post = require('./post')
const image = require('./image')
const user = require('./user')
const userdetails = require('./userdetails')
const series = require('./series')
const comment = require('./comment')
const hashtag = require('./hashtag')
const posthashtag = require('./posthashtag')

const config = require('../config/config')[env]

const db = {}

const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
)

db.Posthashtag = posthashtag
db.Hashtag = hashtag
db.Post = post
db.User = user
db.Image = image
db.Series = series
db.UserDetail = userdetails
db.Comment = comment

Object.keys(db).forEach((modelName) => {
  db[modelName].init(sequelize)
})

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
