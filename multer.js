const multer = require('multer')
const multerS3 = require('multer-s3')
const path = require('path')
const AWS = require('aws-sdk')
require('dotenv').config()

AWS.config.update({
  accessKeyId: process.env.WHALE_S3_ID,
  secretAccessKey: process.env.WHALE_S3_SECRET,
  region: 'ap-northeast-2'
})

const s3Storage = multerS3({
  s3: new AWS.S3(),
  bucket: process.env.WHELE_S3_BUCKET,
  key: (req, file, cb) => {
    cb(null, `original/${Date.now()}_${path.basename(file.originalname)}`)
  }
})

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname, '/uploads'))
  },
  filename: (req, file, cb) => {
    let ext = path.extname(file.originalname)
    if (!ext) {
      if (file.mimetype === 'image/svg+xml') {
        ext = '.svg'
      }
    }
    const basename = path.basename(file.originalname, ext)
    cb(null, `${basename}-${Date.now()}${ext}`)
  },
  limits: { filezise: 20 * 1024 * 1024 }
})

const appliedStorage =
  process.env.NODE_ENV === 'production' ? s3Storage : storage
const upload = multer({ storage: appliedStorage })

module.exports = upload
