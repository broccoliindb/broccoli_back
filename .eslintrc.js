module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true
  },
  extends: ['airbnb-base'],
  parserOptions: {
    ecmaVersion: 12
  },
  rules: {
    semi: 'off',
    'comma-dangle': 'off',
    'arrow-body-style': 'off',
    'object-curly-newline': ['error', { consistent: true }],
    'consistent-return': 'off',
    'operator-linebreak': 'off'
  }
}
