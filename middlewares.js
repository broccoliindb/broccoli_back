module.exports = {
  isLoggedIn: (req, res, next) => {
    if (req.user) {
      return next()
    }
    return res.status(401).json({ message: '로그인이 필요합니다.' })
  },
  isNotLoggedIn: (req, res, next) => {
    if (!req.user) {
      return next()
    }
    return res.status(401).json({ message: '이미 로그인이 되어있습니다.' })
  }
}
