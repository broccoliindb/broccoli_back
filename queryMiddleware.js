const { sequelize } = require('./models')
const Logger = require('./logger')
const {
  getRootCommentStackQueryById,
  getPostCommentsStackQueryByPostKey,
  getHashtagsQueryForNickname,
  getAllSeriesOnPostsQueryForNickname,
  getPostMetaListOnSeriesQueryByPostKey,
  getAllPostsOnConditionQueryByLikeCountAndLastId,
  getAllPostsOnConditionQueryByDateAndLastId
} = require('./queries')

const getResultRawQuery = async (query) => {
  try {
    const options = {
      type: sequelize.QueryTypes.SELECT,
      nest: true
    }
    const result = await sequelize.query(query, options)
    return result
  } catch (err) {
    if (err) {
      Logger.error(err)
    }
  }
}

const getRootComments = async (query) => {
  try {
    const options = {
      type: sequelize.QueryTypes.SELECT
    }
    const comments = await sequelize.query(query, options)

    const setChildComments = (rootComments = [], comment) => {
      const rootIndex = rootComments.findIndex(
        (root) => root.id === comment.upperId
      )
      if (rootIndex !== -1) {
        rootComments[rootIndex].childComments.push({
          ...comment,
          childComments: []
        })
      } else {
        rootComments.forEach((child) => {
          setChildComments(child.childComments, comment)
        })
      }
    }
    const rootComments = []
    comments.forEach((comment) => {
      if (!comment.isDeleted) {
        if (comment.upperId === null) {
          rootComments.push({ ...comment, childComments: [] })
        } else {
          setChildComments(rootComments, comment)
        }
      }
    })
    return rootComments
  } catch (err) {
    Logger.error(err)
  }
}

const getRootCommentStackById = async (id) => {
  try {
    const query = getRootCommentStackQueryById(id)
    const rootComments = await getRootComments(query)
    return rootComments
  } catch (err) {
    Logger.error(err)
  }
}

const getRootCommentStackByPostKey = async (postKey) => {
  try {
    const query = getPostCommentsStackQueryByPostKey(postKey)
    const rootComments = await getRootComments(query)
    return rootComments
  } catch (err) {
    Logger.error(err)
  }
}

const getHashtagsForNickname = async (nickname) => {
  try {
    const query = getHashtagsQueryForNickname(nickname)
    const hashtags = await getResultRawQuery(query)
    return hashtags
  } catch (err) {
    Logger.error(err)
  }
}

const getAllSeriesOnPostsForNickname = async (nickname, updatedAt) => {
  try {
    const query = getAllSeriesOnPostsQueryForNickname(nickname, updatedAt)
    const hashtags = await getResultRawQuery(query)
    return hashtags
  } catch (err) {
    Logger.error(err)
  }
}

const getPostMetaListOnSeriesByPostKey = async (postKey) => {
  try {
    const query = getPostMetaListOnSeriesQueryByPostKey(postKey)
    const postMetaList = await getResultRawQuery(query)
    return postMetaList
  } catch (err) {
    Logger.error(err)
  }
}

const getAllPostsOnConditionByLikeCountAndLastId = async (
  likeCount,
  lastId,
  startDate,
  endDate
) => {
  try {
    const query = getAllPostsOnConditionQueryByLikeCountAndLastId(
      likeCount,
      lastId,
      startDate,
      endDate
    )
    const posts = await getResultRawQuery(query)
    return posts
  } catch (err) {
    Logger.error(err)
  }
}

const getAllPostsOnConditionByDateAndLastId = async (
  lastId,
  startDate,
  endDate
) => {
  try {
    const query = getAllPostsOnConditionQueryByDateAndLastId(
      lastId,
      startDate,
      endDate
    )
    const posts = await getResultRawQuery(query)
    return posts
  } catch (err) {
    Logger.error(err)
  }
}

module.exports = {
  getRootCommentStackById,
  getRootCommentStackByPostKey,
  getHashtagsForNickname,
  getAllSeriesOnPostsForNickname,
  getPostMetaListOnSeriesByPostKey,
  getAllPostsOnConditionByLikeCountAndLastId,
  getAllPostsOnConditionByDateAndLastId
}
