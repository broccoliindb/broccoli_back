const router = require('express').Router()
const Logger = require('../logger')
const { getRootCommentStackByPostKey } = require('../queryMiddleware')

router.get('/:postKey', async (req, res, next) => {
  try {
    const { postKey } = req.params
    const rootComments = await getRootCommentStackByPostKey(postKey)
    return res.status(201).json(rootComments)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
