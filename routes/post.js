const router = require('express').Router()
const { nanoid } = require('nanoid')
const { Post, User, UserDetail, Hashtag, Posthashtag } = require('../models')
const { isLoggedIn } = require('../middlewares')
const Logger = require('../logger')
const { putPost, updatePost } = require('../elasticsearchClient')

router.patch('/:postId/like', isLoggedIn, async (req, res, next) => {
  try {
    const { postId } = req.params
    const post = await Post.findOne({
      where: { id: parseInt(postId, 10) }
    })
    if (!post) {
      return res.status(401).json({ message: '삭제하려는 포스트가 없습니다.' })
    }
    await post.addLiker(req.user.id)
    return res.status(201).json({ postId, userId: req.user.id })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.patch('/:postId/unlike', isLoggedIn, async (req, res, next) => {
  try {
    const { postId } = req.params
    const post = await Post.findOne({
      where: { id: parseInt(postId, 10) }
    })
    if (!post) {
      return res.status(401).json({ message: '삭제하려는 포스트가 없습니다.' })
    }
    await post.removeLiker(req.user.id)
    return res.status(201).json({ postId, userId: req.user.id })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.delete('/:postKey', async (req, res, next) => {
  try {
    const user = await User.findOne({
      where: { id: req.user.id },
      attributes: ['nickname', 'avatar', 'defaultavatar', 'id']
    })
    if (!user) {
      return res.status(401).json({ message: '유저 없습니다.' })
    }
    const { postKey } = req.params
    const post = await Post.findOne({
      where: { postKey }
    })
    if (!post) {
      return res.status(401).json({ message: '삭제하려는 포스트가 없습니다.' })
    }
    await Post.update(
      {
        isTemp: false,
        isUse: false,
        isDeleted: true
      },
      {
        where: { id: post.id, userId: req.user.id }
      }
    )
    const updatedPost = await Post.findOne({
      where: { postKey }
    })
    await updatePost(updatedPost, user)
    await Post.destroy({
      where: {
        postKey
      }
    })
    return res.status(200).json({ message: '삭제가 완료되었습니다.' })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})
router.get('/:postKey', async (req, res, next) => {
  try {
    const { postKey } = req.params
    const post = await Post.findOne({
      where: { postKey, isDeleted: false }
    })
    const hashWhere = {}
    if (post.isTemp && !post.isUse) {
      hashWhere.isTemp = true
      hashWhere.isTempDeleted = false
    } else {
      hashWhere.isUse = true
    }
    const finalPost = await Post.findOne({
      where: { postKey, isDeleted: false },
      include: [
        {
          model: User,
          attributes: [
            'id',
            'nickname',
            'avatar',
            'email',
            'defaultavatar',
            'blogname'
          ],
          include: [{ model: UserDetail, attributes: ['whoami'] }]
        },
        {
          model: User,
          as: 'Liker',
          attributes: ['id'],
          through: {
            attributes: []
          }
        },
        {
          model: Hashtag,
          attributes: ['tag', 'id'],
          through: {
            attributes: [],
            where: hashWhere
          }
        }
      ]
    })
    if (!finalPost) {
      return res.status(200).json(null)
    }
    return res.status(200).json(finalPost)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})
router.get('/temp/:postKey', async (req, res, next) => {
  try {
    const { postKey } = req.params
    const post = await Post.findOne({
      attributes: [
        'id',
        'title',
        'content',
        'postKey',
        'markdown',
        'isTemp',
        'tempTitle',
        'tempContent',
        'tempMarkdown',
        'isUse',
        'isDeleted',
        'scope',
        'summary',
        'series',
        'thumbnail',
        'updatedAt',
        'userId',
        'seriesId'
      ],
      where: { postKey, isDeleted: false },
      include: [
        {
          model: User,
          attributes: [
            'id',
            'nickname',
            'avatar',
            'email',
            'defaultavatar',
            'blogname'
          ],
          include: [{ model: UserDetail, attributes: ['whoami'] }]
        },
        {
          model: User,
          as: 'Liker',
          attributes: ['id'],
          through: {
            attributes: []
          }
        },
        {
          model: Hashtag,
          attributes: ['tag', 'id'],
          through: {
            attributes: [],
            where: { isTemp: true, isTempDeleted: false }
          }
        }
      ]
    })
    if (!post) {
      return res.status(200).json(null)
    }
    return res.status(200).json(post)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})
router.post('/', isLoggedIn, async (req, res, next) => {
  try {
    const {
      id,
      title,
      content,
      markdown,
      scope,
      summary,
      series,
      thumbnail,
      tags,
      deletedTags
    } = req.body
    const user = await User.findOne({
      where: { id: req.user.id },
      attributes: ['nickname', 'avatar', 'defaultavatar', 'id']
    })
    if (!user) {
      return res.status(401).json({ message: '유저 없습니다.' })
    }
    if (id !== -1) {
      const post = await Post.findOne({
        where: { id, userId: req.user.id }
      })
      if (!post) {
        return res.status(401).json({ message: '본인이 작성한 글이 아닙니다.' })
      }
      if (deletedTags && deletedTags.length) {
        await post.removeHashtags(deletedTags.map((i) => i.id))
      } else {
        const unUsedTags = await post.getHashtags({
          through: { where: { isUse: false } }
        })
        await post.removeHashtags(unUsedTags)
      }
      if (tags) {
        const hashtags = await Promise.all(
          tags.map(async (item) => {
            const newTag = await Hashtag.findOrCreate({
              where: { tag: item.tag.toLowerCase() }
            })
            return newTag
          })
        )
        await Promise.all(
          hashtags.map(async (tagInfo) => {
            const [tag] = tagInfo
            let addedTag = null
            const isExist = await tag.hasPost(post)
            if (!isExist) {
              addedTag = await tag.addPost(post, {
                through: { isTemp: false, isUse: true }
              })
            } else {
              addedTag = await Posthashtag.update(
                { isTemp: false, isTempDeleted: false, isUse: true },
                {
                  where: { postId: post.id, hashtagId: tag.id }
                }
              )
            }
            return addedTag
          })
        )
      }
      await Post.update(
        {
          title,
          content,
          markdown,
          tempTitle: null,
          tempContent: null,
          tempMarkdown: null,
          isTemp: false,
          isUse: true,
          scope,
          summary,
          series: series?.name,
          seriesId: series?.id,
          thumbnail
        },
        {
          where: { id, userId: req.user.id }
        }
      )
      const updatedPost = await Post.findOne({
        where: { id, userId: req.user.id },
        include: [
          { model: User, as: 'Liker' },
          {
            model: Hashtag,
            attributes: ['tag', 'id'],
            through: {
              attributes: [],
              where: { isUse: true }
            }
          }
        ]
      })
      await updatePost(updatedPost, user)
      return res.status(200).json(updatedPost)
    }
    const createdPost = await Post.create({
      postKey: nanoid(),
      title,
      content,
      markdown,
      userId: req.user.id,
      isTemp: false,
      isUse: true,
      scope,
      summary,
      series: series?.name,
      seriesId: series?.id,
      thumbnail
    })
    if (tags) {
      const hashtags = await Promise.all(
        tags.map(async (item) => {
          const newTag = await Hashtag.findOrCreate({
            where: { tag: item.tag.toLowerCase() }
          })
          return newTag
        })
      )
      hashtags.forEach(async (tagInfo) => {
        const [tag] = tagInfo
        await tag.addPost(createdPost)
      })
    }
    const post = await Post.findOne({
      where: { id: createdPost.id, userId: req.user.id },
      include: [
        {
          model: User,
          as: 'Liker',
          attributes: ['id'],
          through: {
            attributes: []
          }
        },
        {
          model: Hashtag,
          attributes: ['tag', 'id'],
          through: {
            attributes: [],
            where: { isUse: true }
          }
        }
      ]
    })
    await putPost(post, user)
    return res.status(201).json(post)
  } catch (err) {
    if (err) {
      Logger.error(err)
      return next(err)
    }
  }
})
router.post('/temp', isLoggedIn, async (req, res, next) => {
  try {
    const { id, title, content, markdown, tags, deletedTags } = req.body
    if (id === -1) {
      const key = nanoid()
      const tempPost = await Post.create({
        postKey: key,
        tempTitle: title,
        tempContent: content,
        tempMarkdown: markdown,
        userId: req.user.id,
        isTemp: true,
        isUse: false
      })
      if (tags) {
        const hashtags = await Promise.all(
          tags.map(async (item) => {
            const newTag = await Hashtag.findOrCreate({
              where: { tag: item.tag.toLowerCase() }
            })
            return newTag
          })
        )
        hashtags.forEach(async (tagInfo) => {
          const [tag] = tagInfo
          await tag.addPost(tempPost, {
            through: { isTemp: true, isUse: false }
          })
        })
      }
      const updatedTempPost = await Post.findOne({
        where: { id: tempPost.id, userId: req.user.id },
        include: [
          {
            model: User,
            as: 'Liker',
            attributes: ['id'],
            through: {
              attributes: []
            }
          },
          {
            model: Hashtag,
            attributes: ['tag', 'id'],
            through: {
              attributes: [],
              where: { isTemp: true, isTempDeleted: false }
            }
          }
        ]
      })
      return res.status(200).json(updatedTempPost)
    }
    const tmpPost = await Post.findOne({
      where: { id, userId: req.user.id }
    })
    if (!tmpPost) {
      return res.status(401).json({ message: '본인이 작성한 글이 아닙니다.' })
    }
    if (deletedTags && deletedTags.length) {
      await tmpPost.addHashtags(
        deletedTags.map((i) => i.id),
        {
          through: { isTempDeleted: true }
        }
      )
    }
    const pastTempTags = await tmpPost.getHashtags({
      through: { where: { isTemp: true, isUse: false } }
    })
    await tmpPost.removeHashtags(pastTempTags)

    if (tags) {
      const hashtags = await Promise.all(
        tags.map(async (item) => {
          const newTag = await Hashtag.findOrCreate({
            where: { tag: item.tag.toLowerCase() }
          })
          return newTag
        })
      )
      await Promise.all(
        hashtags.map(async (tagInfo) => {
          const [tag] = tagInfo
          let addedTag = null
          const isExist = await tag.hasPost(tmpPost)
          if (!isExist) {
            addedTag = await tag.addPost(tmpPost, {
              through: { isTemp: true, isUse: false }
            })
          } else {
            addedTag = await Posthashtag.update(
              { isTemp: true, isTempDeleted: false },
              {
                where: { postId: tmpPost.id, hashtagId: tag.id }
              }
            )
          }
          return addedTag
        })
      )
    }
    await Post.update(
      {
        tempTitle: title,
        tempContent: content,
        tempMarkdown: markdown,
        isTemp: true
      },
      {
        where: { id, userId: req.user.id }
      }
    )
    const updatedTempPost = await Post.findOne({
      where: { id, userId: req.user.id },
      include: [
        {
          model: User,
          as: 'Liker',
          attributes: ['id'],
          through: {
            attributes: []
          }
        },
        {
          model: Hashtag,
          attributes: ['tag', 'id'],
          through: {
            attributes: [],
            where: { isTemp: true, isTempDeleted: false }
          }
        }
      ]
    })
    return res.status(200).json(updatedTempPost)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
