const router = require('express').Router()
const Logger = require('../logger')
const {
  getSearchPosts,
  getSearchTermTokens
} = require('../elasticsearchClient')

router.post('/', async (req, res, next) => {
  try {
    const { q, username, userId } = req.body
    const result = await getSearchPosts(q, username, userId)
    const {
      body: {
        hits: { total, hits }
      }
    } = result
    res.json({ total, hits })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.get('/terms', async (req, res, next) => {
  try {
    const { term } = req.query
    const result = await getSearchTermTokens(term)
    const {
      body: { tokens }
    } = result
    const results = tokens.map((i) => {
      return { token: i.token.split('/')[0] }
    })
    res.json(results)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
