const router = require('express').Router()
const { isLoggedIn } = require('../middlewares')
const { Series, Post } = require('../models')
const Logger = require('../logger')

router.get('/:seriesId', async (req, res, next) => {
  try {
    const { seriesId } = req.params
    const id = parseInt(seriesId, 10)
    const series = await Series.findOne({
      where: { id },
      include: {
        model: Post,
        attributes: ['id', 'title', 'postkey'],
        where: { isDeleted: false, isUse: true }
      }
    })
    res.status(201).json(series)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.get('/', isLoggedIn, async (req, res, next) => {
  try {
    const series = await Series.findAll({
      where: { userId: req.user.id }
    })
    res.status(200).json(series)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})
router.post('/', isLoggedIn, async (req, res, next) => {
  try {
    const addedSeries = await Series.create({
      name: req.body.name,
      userId: req.body.userId
    })
    res.status(200).json(addedSeries)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
