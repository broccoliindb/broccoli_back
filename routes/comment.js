const router = require('express').Router()
const Logger = require('../logger')
const { Comment, User, Post } = require('../models')
const { getRootCommentStackById } = require('../queryMiddleware')
const { isLoggedIn } = require('../middlewares')

router.patch('/:id', isLoggedIn, async (req, res, next) => {
  try {
    const { content } = req.body
    const id = parseInt(req.params.id, 10)
    if (Number.isNaN(id)) {
      return res.status(401).json({ message: '삭제하려는 댓글 id가 없습니다.' })
    }
    await Comment.update({ content }, { where: { id, userId: req.user.id } })
    const root = await getRootCommentStackById(id)
    return res.status(201).json(...root)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.delete('/:id', isLoggedIn, async (req, res, next) => {
  try {
    const childLength = parseInt(req.query.childLength, 10)
    const id = parseInt(req.params.id, 10)
    if (Number.isNaN(id)) {
      return res.status(401).json({ message: '삭제하려는 댓글 id가 없습니다.' })
    }
    if (!Number.isNaN(childLength) && childLength > 0) {
      return res
        .status(401)
        .json({ message: '댓글이 달린글은 삭제할 수 없습니다.' })
    }

    await Comment.update(
      { isDeleted: true },
      { where: { id, userId: req.user.id } }
    )
    const root = await getRootCommentStackById(id)
    await Comment.destroy({
      where: { id, userId: req.user.id }
    })
    return res.status(201).json(...root)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.post('/', isLoggedIn, async (req, res, next) => {
  try {
    const { postId, content, upperId } = req.body
    const user = await User.findOne({
      where: { id: req.user.id },
      attributes: ['id']
    })
    if (!user) {
      return res.status(401).json({ message: '유저 없습니다.' })
    }
    const post = await Post.findOne({
      where: { id: postId },
      attributes: ['id']
    })
    if (!post) {
      return res.status(401).json({ message: '해당 포스트가 없습니다.' })
    }
    const newComment = await Comment.create({
      userId: req.user.id,
      postId,
      content,
      upperId,
      isDeleted: false
    })
    const root = await getRootCommentStackById(newComment.id)
    return res.status(201).json(...root)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
