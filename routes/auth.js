const router = require('express').Router()
const passport = require('passport')
const bcrypt = require('bcrypt')
const jwtDecode = require('jwt-decode')
const { User, UserDetail } = require('../models')
const { FRONT_URL } = require('../config/environments')
const CustomError = require('../custom-error')
const { isLoggedIn, isNotLoggedIn } = require('../middlewares')
const Logger = require('../logger')
/**
 * LOCAL SIGNUP
 */
router.post('/signup', isNotLoggedIn, async (req, res, next) => {
  try {
    const { email, password, defaultavatar } = req.body
    const user = await User.findOne({
      where: { email },
      attributes: ['id']
    })
    if (user) {
      throw new CustomError(401, '이미 존재하는 이메일입니다. 다시 시도하세요.')
    }
    if (email.length > 30) {
      throw new CustomError(401, '이메일은 30자이내로 해주세요.')
    }
    const hashedPassword = await bcrypt.hash(
      password,
      parseInt(process.env.SALT_ROUNDS, 10)
    )
    await User.create({
      email,
      password: hashedPassword,
      defaultavatar,
      nickname: email.split('@')[0],
      visitCount: 1
    })
    const createdUser = await User.findOne({
      where: { email },
      attributes: [
        'id',
        'nickname',
        'avatar',
        'email',
        'defaultavatar',
        'visitCount',
        'blogname'
      ],
      include: [{ model: UserDetail, attributes: ['whoami'] }]
    })
    req.login(createdUser, (err) => {
      if (err) {
        Logger.error(err)
        return next(err)
      }
      req.session.save((error) => {
        if (error) {
          Logger.error(error)
          return next(error)
        }
        res.status(200).json(createdUser)
      })
    })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

/**
 * LOCAL LOGIN
 */
router.post('/login', isNotLoggedIn, (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      Logger.error(err)
      return next(err)
    }
    if (info) {
      return res.status(401).json(info)
    }
    if (user) {
      req.login(user, async (error) => {
        if (error) {
          Logger.error(error)
          return next(error)
        }
        try {
          await User.update(
            {
              visitCount: user.visitCount + 1
            },
            {
              where: { id: user.id }
            }
          )
          const userWithoutPassword = await User.findOne({
            where: { id: user.id },
            attributes: [
              'id',
              'nickname',
              'avatar',
              'email',
              'defaultavatar',
              'visitCount',
              'blogname'
            ],
            include: [{ model: UserDetail, attributes: ['whoami'] }]
          })
          res.status(200).json(userWithoutPassword)
        } catch (e) {
          if (e) {
            Logger.error(e)
            next(e)
          }
        }
      })
    }
  })(req, res, next)
})

/**
 * LOGOUT
 */
router.get('/logout', isLoggedIn, (req, res) => {
  req.logout()
  req.session.destroy()
  res.send('ok')
})

/**
 * GITHUB SIGNUP/LOGIN
 */
router.get('/github', isNotLoggedIn, (req, res, next) => {
  passport.authenticate('github', {
    scope: ['user:email']
  })(req, res, next)
})

/**
 * GITHUB CALLBACK
 */
router.get(
  '/github/callback',
  isNotLoggedIn,
  passport.authenticate('github', { failureRedirect: `${FRONT_URL}` }),
  (req, res) => {
    const { visitCount } = req.user

    if (visitCount === 1) {
      return res.redirect(`${FRONT_URL}/settings`)
    }
    res.redirect(`${FRONT_URL}?isLoggedSuccess=true`)
  }
)

/**
 * GOOGLE SIGNUP/LOGIN
 */
router.get('/google', isNotLoggedIn, (req, res, next) => {
  passport.authenticate('google', {
    scope: ['email', 'profile']
  })(req, res, next)
})

/**
 * GOOGLE CALLBACK
 */
router.get(
  '/google/callback',
  isNotLoggedIn,
  passport.authenticate('google', { failureRedirect: `${FRONT_URL}` }),
  (req, res) => {
    const { visitCount } = req.user

    if (visitCount === 1) {
      return res.redirect(`${FRONT_URL}/settings`)
    }
    res.redirect(`${FRONT_URL}?isLoggedSuccess=true`)
  }
)

/**
 * KAKAO SIGNUP/LOGIN
 */
router.get('/kakao', isNotLoggedIn, (req, res, next) => {
  passport.authenticate('kakao')(req, res, next)
})

/**
 * KAKAO CALLBACK
 */
router.get('/kakao/callback', isNotLoggedIn, (req, res, next) => {
  passport.authenticate('kakao', (err, user, info) => {
    if (err) {
      Logger.error(err)
      next(err)
    }
    if (info) {
      if (info.redirect) {
        return res.redirect(info.redirect)
      }
      return res.status(401).json(info)
    }
    if (user) {
      req.login(user, async (error) => {
        if (error) {
          Logger.error(error)
          return next(error)
        }
        try {
          await User.update(
            {
              visitCount: user.visitCount + 1
            },
            {
              where: { id: user.id }
            }
          )
        } catch (e) {
          if (e) {
            Logger.error(e)
            next(e)
          }
        }
      })
      const { visitCount } = req.user
      if (visitCount === 1) {
        return res.redirect(`${FRONT_URL}/settings`)
      }
      res.redirect(`${FRONT_URL}?isLoggedSuccess=true`)
    }
  })(req, res, next)
})

/**
 * FACEBOOK SIGNUP/LOGIN
 */
router.get('/facebook', isNotLoggedIn, (req, res, next) => {
  passport.authenticate('facebook', {
    scope: ['email', 'public_profile']
  })(req, res, next)
})

/**
 * FACEBOOK CALLBACK
 */
router.get(
  '/facebook/callback',
  isNotLoggedIn,
  passport.authenticate('facebook', { failureRedirect: `${FRONT_URL}` }),
  (req, res) => {
    const { visitCount } = req.user

    if (visitCount === 1) {
      return res.redirect(`${FRONT_URL}/settings`)
    }
    res.redirect(`${FRONT_URL}?isLoggedSuccess=true`)
  }
)

// 비밀번호변경시
router.get('/', async (req, res, next) => {
  try {
    const { token } = req.query
    if (!token) return res.status(401).json({ message: '토큰이 없습니다.' })
    res.status(200).redirect(`${FRONT_URL}/settings?token=${token}`)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

// 비밀번호변경시
router.get('/check', async (req, res, next) => {
  try {
    const { token } = req.query
    if (!token) {
      return res.status(401).json({ message: '토큰이 없습니다.' })
    }
    const { email, exp } = jwtDecode(token)
    const user = await User.findOne({
      where: { email },
      attributes: [
        'id',
        'nickname',
        'avatar',
        'email',
        'defaultavatar',
        'visitCount',
        'blogname'
      ],
      include: [{ model: UserDetail, attributes: ['whoami'] }]
    })
    if (!user) {
      return res.status(401).json({ message: '해당하는 사용자가 없습니다.' })
    }
    const userWithToken = await User.findOne({
      where: { confirmationCode: token },
      attributes: ['id']
    })
    if (!userWithToken) {
      return res.status(401).json({ message: '유효하지 않은 토큰입니다.' })
    }
    if (new Date() > new Date(exp * 1000)) {
      return res.status(401).json({ message: '유효시간이 지났습니다.' })
    }
    req.login(user, (err) => {
      if (err) {
        Logger.error(err)
        return next(err)
      }
      req.session.save((error) => {
        if (error) {
          Logger.error(error)
          return next(error)
        }
        res.status(200).json(user)
      })
    })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
