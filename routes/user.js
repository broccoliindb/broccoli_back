const router = require('express').Router()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const { User, UserDetail } = require('../models')
const { isLoggedIn } = require('../middlewares')
const { sendSecretMail } = require('../mailservice')
const Logger = require('../logger')

require('dotenv').config()

router.delete('/:userId', isLoggedIn, async (req, res, next) => {
  try {
    Logger.debug(req.params)
    const { userId } = req.params
    if (userId) {
      await User.destroy({
        where: {
          id: parseInt(userId, 10)
        }
      })
      return res.status(200).json({ message: '탈퇴가 완료되었습니다.' })
    }
    return res.status(401).json({ message: '탈퇴진행중 문제가 발생했습니다.' })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})
router.patch('/:userId/update', isLoggedIn, async (req, res, next) => {
  try {
    const { avatar, nickname, password, whoami, blogname } = req.body
    const result = { message: '', user: null }
    if (typeof blogname !== 'undefined') {
      result.message = '블로그명이 변경되었습니다.'
      await User.update(
        {
          blogname
        },
        {
          where: { id: req.user.id }
        }
      )
    }
    if (typeof avatar !== 'undefined') {
      result.message = avatar
        ? '아바타가 변경되었습니다.'
        : '디폴트 아바타가 적용됩니다.'
      await User.update(
        {
          avatar
        },
        {
          where: { id: req.user.id }
        }
      )
    }
    if (typeof nickname !== 'undefined') {
      result.message = '사용자정보가 업데이트 변경되었습니다.'
      await User.update(
        {
          nickname
        },
        {
          where: { id: req.user.id }
        }
      )
    }
    if (typeof password !== 'undefined') {
      result.message = '비밀번호가 변경되었습니다.'
      const hashedPassword = await bcrypt.hash(
        password,
        parseInt(process.env.SALT_ROUNDS, 10)
      )
      await User.update(
        {
          password: hashedPassword
        },
        {
          where: { id: req.user.id }
        }
      )
    }
    if (typeof whoami !== 'undefined') {
      result.message = '사용자정보가 업데이트 변경되었습니다.'
      const detail = await UserDetail.findOne({
        where: { userId: req.user.id }
      })
      if (detail) {
        await UserDetail.update(
          {
            whoami
          },
          {
            where: { userId: req.user.id }
          }
        )
      } else {
        await UserDetail.create({
          whoami,
          userId: req.user.id
        })
      }
    }
    const user = await User.findOne({
      where: {
        id: req.user.id
      },
      attributes: [
        'id',
        'nickname',
        'avatar',
        'email',
        'defaultavatar',
        'blogname'
      ],
      include: [{ model: UserDetail, attributes: ['whoami'] }]
    })
    result.user = user
    return res.status(200).json(result)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})
router.get('/email', async (req, res, next) => {
  try {
    const { email } = req.query
    const user = await User.findOne({
      where: { email },
      attributes: ['id']
    })
    if (!user) {
      return res.status(401).json({ message: '사용자가 존재하지 않습니다.' })
    }
    const token = jwt.sign({ email }, process.env.SECRET, { expiresIn: 60 * 5 })
    await User.update(
      { confirmationCode: token },
      {
        where: { email }
      }
    )
    sendSecretMail(email, token)
    res.status(200).json({
      message: '비밀번호갱신토큰을 발행했습니다. 이메일을 확인해주세요.',
      token
    })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})
router.get('/userInfo', async (req, res, next) => {
  try {
    const { userId, nickname } = req.query
    const decodedNickname = decodeURIComponent(nickname)
    const where = {}
    if (nickname) {
      if (userId) {
        where.id = parseInt(userId, 10)
      } else {
        where.nickname = decodedNickname
      }
      const user = await User.findOne({
        where,
        attributes: ['nickname', 'blogname', 'avatar', 'defaultavatar'],
        include: [{ model: UserDetail, attributes: ['whoami'] }]
      })
      res.status(200).json(user)
    } else {
      res.status(200).json(null)
    }
  } catch (e) {
    if (e) {
      Logger.error(e)
      next(e)
    }
  }
})

router.get('/', async (req, res, next) => {
  try {
    if (req.user) {
      const user = await User.findOne({
        where: { id: req.user.id },
        attributes: [
          'id',
          'nickname',
          'avatar',
          'email',
          'defaultavatar',
          'blogname'
        ],
        include: [{ model: UserDetail, attributes: ['whoami'] }]
      })
      res.status(200).json(user)
    } else {
      res.status(200).json(null)
    }
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})
module.exports = router
