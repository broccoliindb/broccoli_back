const express = require('express')
const fs = require('fs')
const path = require('path')
const multer = require('../multer')
const { BACK_URL } = require('../config/environments')
const Logger = require('../logger')
require('dotenv').config()

try {
  fs.accessSync('uploads')
} catch (err) {
  fs.mkdirSync('uploads')
}

const router = express.Router()
router.post('/image', multer.single('image'), async (req, res, next) => {
  try {
    const ext = path.extname(req.file.originalname)
    const basename = path.basename(req.file.originalname, ext)
    const srcPath =
      process.env.NODE_ENV === 'production'
        ? req.file.location
        : `${BACK_URL}/${req.file.filename}`
    return res.status(200).json({
      src: srcPath,
      name: basename
    })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.post('/avatar', multer.single('image'), async (req, res, next) => {
  try {
    const ext = path.extname(req.file.originalname)
    const basename = path.basename(req.file.originalname, ext)
    return res.status(200).json({
      src: `${BACK_URL}/${req.file.filename}`,
      name: basename
    })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
