const router = require('express').Router()
const { Op } = require('sequelize')
const {
  getAllSeriesOnPostsForNickname,
  getPostMetaListOnSeriesByPostKey,
  getAllPostsOnConditionByLikeCountAndLastId,
  getAllPostsOnConditionByDateAndLastId
} = require('../queryMiddleware')
const { Post, User, Comment, Hashtag } = require('../models')
const {
  getFirstLastDateOfMonth,
  getFirstLastDateOfWeek,
  getFirstLastOfDate
} = require('../dateUtils')
const Logger = require('../logger')

const LOAD_TYPE = {
  IN_USE: 'IN_USE',
  IN_TEMP: 'IN_TEMP'
}

router.get('/hashtags/:hashtag', async (req, res, next) => {
  try {
    const { hashtag } = req.params
    const { lastId } = req.query
    const decodedHashtag = decodeURIComponent(hashtag)
    const where = { isDeleted: false, isUse: true }
    const postId = parseInt(lastId, 10)
    if (Number.isInteger(postId) && postId > 0) {
      where.id = { [Op.lt]: postId }
    }
    const posts = await Post.findAll({
      order: [['id', 'DESC']],
      where,
      limit: 10,
      include: [
        {
          model: Hashtag,
          attributes: ['tag', 'id'],
          through: {
            attributes: [],
            where: { isUse: true }
          },
          where: { tag: decodedHashtag }
        },
        { model: User, attributes: ['nickname', 'avatar', 'defaultavatar'] },
        { model: Comment, attributes: ['id'] },
        {
          model: User,
          as: 'Liker',
          attributes: ['id'],
          through: {
            attributes: []
          }
        }
      ]
    })
    res.status(201).json(posts)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.get('/series/:seriesName', async (req, res, next) => {
  try {
    const { seriesName } = req.params
    const { seriesId, updatedAt } = req.query
    const where = { isDeleted: false, isUse: true }
    const id = parseInt(seriesId, 10)
    if (id === 0) {
      where.seriesId = null
    }
    if (id !== 0 && Number.isInteger(id)) {
      where.seriesId = id
      where.series = decodeURIComponent(seriesName)
    }
    if (updatedAt !== '0') {
      where.updatedAt = { [Op.lt]: updatedAt }
    }
    const posts = await Post.findAll({
      order: [['updatedAt', 'DESC']],
      where,
      limit: 10,
      include: [
        {
          model: User,
          attributes: ['nickname', 'avatar', 'defaultavatar']
        },
        {
          model: User,
          as: 'Liker',
          attributes: ['id'],
          through: {
            attributes: []
          }
        },
        {
          model: Comment,
          attributes: ['id']
        },
        {
          model: Hashtag,
          attributes: ['tag', 'id'],
          through: {
            attributes: [],
            where: { isUse: true }
          }
        }
      ]
    })
    res.status(201).json(posts)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.get('/series', async (req, res, next) => {
  try {
    const { updatedAt, nickname, postKey } = req.query
    const decodedNickname = decodeURIComponent(nickname)
    let posts = []
    if (postKey) {
      posts = await getPostMetaListOnSeriesByPostKey(postKey)
    }
    if (nickname && updatedAt) {
      posts = await getAllSeriesOnPostsForNickname(decodedNickname, updatedAt)
    }
    res.status(201).json(posts)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.get('/', async (req, res, next) => {
  try {
    const { orderBy, durationType, lastId, likeCount } = req.query
    let startDate = null
    let endDate = null
    switch (durationType) {
      case 'M': {
        const today = new Date()
        const [start, end] = getFirstLastDateOfMonth(today)
        startDate = start
        endDate = end
        break
      }
      case 'W': {
        const today = new Date()
        const [start, end] = getFirstLastDateOfWeek(today)
        startDate = start
        endDate = end
        break
      }
      case 'T': {
        const today = new Date()
        const [start, end] = getFirstLastOfDate(today)
        startDate = start
        endDate = end
        break
      }
      default:
    }

    if (orderBy === 'DATE') {
      const posts = await getAllPostsOnConditionByDateAndLastId(
        parseInt(lastId, 10),
        startDate && startDate.toISOString(),
        endDate && endDate.toISOString()
      )
      return res.status(200).json(posts)
    }
    const posts = await getAllPostsOnConditionByLikeCountAndLastId(
      parseInt(likeCount, 10),
      parseInt(lastId, 10),
      startDate && startDate.toISOString(),
      endDate && endDate.toISOString()
    )
    return res.status(200).json(posts)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

router.get('/:loadType', async (req, res, next) => {
  try {
    const { updatedAt, authorId, nickname } = req.query
    const { loadType } = req.params
    const whereForUser = {}
    const userId = parseInt(authorId, 10)
    const decodedNickname = decodeURIComponent(nickname || '')
    if (!nickname && loadType === LOAD_TYPE.IN_TEMP) {
      whereForUser.id = req.user ? req.user.id : -1
    } else if (authorId) {
      whereForUser.id = userId
    }
    if (nickname) {
      whereForUser.nickname = decodeURIComponent(nickname)
    }
    const user = await User.findOne({
      where: whereForUser,
      attributes: ['id']
    })
    if (!user) {
      return res.status(200).json({
        results: [],
        message: '로그인사용자가 없습니다. 홈으로 이동합니다.'
      })
    }
    const where = { isDeleted: false, userId: user.id }
    const hashWhere = {}
    if (updatedAt !== '0') {
      where.updatedAt = { [Op.lt]: updatedAt }
    }
    if (
      loadType === LOAD_TYPE.IN_USE &&
      (!req.user || req.user.nickname !== decodedNickname)
    ) {
      where.scope = 'A'
    }
    if (loadType === LOAD_TYPE.IN_USE) {
      where.isUse = true
      hashWhere.isUse = true
    } else if (loadType === LOAD_TYPE.IN_TEMP) {
      where.isTemp = true
      hashWhere.isTemp = true
      hashWhere.isTempDeleted = false
    }
    const posts = await Post.findAll({
      order: [['updatedAt', 'DESC']],
      where,
      limit: 10,
      include: [
        {
          model: User,
          attributes: ['nickname', 'avatar', 'defaultavatar']
        },
        {
          model: User,
          as: 'Liker',
          attributes: ['id'],
          through: {
            attributes: []
          }
        },
        {
          model: Comment,
          attributes: ['id']
        },
        {
          model: Hashtag,
          attributes: ['tag', 'id'],
          through: {
            attributes: [],
            where: hashWhere
          }
        }
      ]
    })
    return res.status(200).json({ results: posts, message: '' })
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
