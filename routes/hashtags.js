const router = require('express').Router()
const { getHashtagsForNickname } = require('../queryMiddleware')
const Logger = require('../logger')

router.get('/:nickname', async (req, res, next) => {
  try {
    const { nickname } = req.params
    const hashtags = await getHashtagsForNickname(nickname)
    return res.status(201).json(hashtags)
  } catch (err) {
    if (err) {
      Logger.error(err)
      next(err)
    }
  }
})

module.exports = router
