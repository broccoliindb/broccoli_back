const nodemailer = require('nodemailer')
const mg = require('nodemailer-mailgun-transport')
const Logger = require('./logger')
const { FRONT_URL, BACK_URL } = require('./config/environments')

const sendMail = (email) => {
  const options = {
    auth: {
      api_key: process.env.MAILGUN_KEY,
      domain: process.env.MAILGUN_DOMAIN
    }
  }
  const client = nodemailer.createTransport(mg(options))
  return client
    .sendMail(email)
    .then(() => {
      Logger.debug('✅ Message Sent!!')
    })
    .catch((error) => Logger.error(error))
}

const sendSecretMail = (address, secret) => {
  const email = {
    from: 'noreply@whale.com',
    to: address,
    subject: 'Login with this Link',
    html: `<main>
  <Header>
    <h1><image src='${FRONT_URL}/favicon-32x32.png'><span style='padding-left:1rem'>Whale<span></h1>
  </Header>
  <p>
  broccoli의 비밀번호변경을 위해서는 아래 버튼을 통해 비밀번호 변경을 해주세요. 5분 이후에는 토큰이 만료됩니다.
  </p>
  <div style='text-align:center'>
    <a href=${BACK_URL}/auth?token=${secret}>
      <button style='padding:0.5rem; border-radius:5px; border:none; background-color:#8ABBA8; color:white; font-weight:bold'>비밀번호변경</button>
    </a>
  </div>
</main>
    `
  }
  return sendMail(email)
}

module.exports = {
  sendSecretMail
}
