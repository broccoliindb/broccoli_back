const CustomError = require('./custom-error')
const Logger = require('./logger')

module.exports = (err, req, res, next) => {
  if (err instanceof CustomError) {
    const error = { message: err.message, stack: err.stack }
    return res.status(err.code).json(error)
  }
  Logger.error('😎😎😎', err)
  next(err)
}
