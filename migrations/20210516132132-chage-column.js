'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('users', 'confirmationCode', {
      type: Sequelize.STRING(200),
      allowNull: true
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('users', 'confirmationCode', {
      type: Sequelize.STRING(100),
      allowNull: true
    })
  }
}
